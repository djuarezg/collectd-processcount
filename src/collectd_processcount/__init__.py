import re
import subprocess
import types
import uuid
from collections import defaultdict
from threading import Timer

import collectd
import psutil
import yaml

PLUGIN_NAME = 'processcount'
FILTERS = ['ppid', 'uid', 'command_line']
LEGACY_PPID_FILTER_NAME = 'legacy-ppid'

CONFIG_DEFAULT_INTERVAL = -1


class ProcesscountConfig(object):
    """Class containing the default values for the configuration parameters of the processcount collectd plugin"""
    def __init__(self):
        self.processes = defaultdict(lambda: defaultdict(lambda: defaultdict(list)))
        self.java_processes = []
        self.interval = CONFIG_DEFAULT_INTERVAL
        self.config_name = uuid.uuid4().hex


def _get_attr_or_method(obj, name):
    """
    psutil changed the interface and depending on the version
    a process name can be an attribute or a function.
    :param obj: The object to get the data from
    :type obj: object
    :param name: The attribute's or method's name whose value is sought
    :type name: str
    :returns: The value of the attribute or returned by the method specified
    """

    attr = getattr(obj, name)
    is_method = hasattr(obj, name) and isinstance(attr, types.MethodType)
    return attr() if is_method else attr


def _get_all_running_java_processes_args_iter():
    """
    Execute bash "jps" command in order to get the java process arguments.
     In case the command does not respond, it will be killed after the timeout.
    :return: Tuple of running java processes with their arguments
    """

    timeout_sec = 10
    timeout_timer = None

    try:
        proc = subprocess.Popen(['jps', '-m'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
        timeout_timer = Timer(timeout_sec, lambda process: process.kill(), [proc])
        timeout_timer.start()
        outs, errs = proc.communicate()
    except OSError as os_error:
        collectd.warning('%s plugin: [Error] Exception on "jps -m" execute. Java devtools are not installed on this machine. %s' % (PLUGIN_NAME, os_error))
        return list()
    except Exception as e:
        collectd.warning('%s plugin: [Error] Exception while getting all running Java processes %s' % (PLUGIN_NAME, e))
        return list()
    finally:
        if timeout_timer is not None:
            timeout_timer.cancel()

    if errs:
        collectd.warning('%s plugin: Errors in "jps -m" output: %s' % (PLUGIN_NAME, errs))
        return list()
    return outs.strip().split('\n')


def _get_process_name_from_regex(process_regex):
    return re.sub('[^a-zA-Z0-9_-]', '', process_regex)


def _is_valid_system_process(process, process_name, filters):
    """Checks if a system process meets the filtering criteria specified for it

    :param process: process to be checked
    :type process: Process
    :param process_name: Expected name for the process
    :type process_name: str
    :param filters: Conditions that the the process ought to meet
    :type filters: dict[str, list[int]]
    :returns: Whether the process meet the specified criteria or not
    :rtype: bool"""

    if _get_attr_or_method(process, 'name') != process_name:
        return False

    attributes = {
        "uid":  int(_get_attr_or_method(process, 'uids').effective),
        "ppid": _get_attr_or_method(process, 'ppid'),
        "command_line": ' '.join(_get_attr_or_method(process, 'cmdline'))
    }

    for filter_name, filter_values in filters.items():
        if filter_name == 'command_line':
            match = False
            for command_line_regex in filter_values:
                if re.search(command_line_regex, attributes[filter_name]):
                    match = True
                    break

            if not match:
                return False
        elif attributes[filter_name] not in filter_values:
            return False

    return True


def _dispatch_system_processes(config, val):
    """Makes val dispatch all system processes that meet the criteria specified in config"""
    for process_name, filter_sets in config.processes.items():
        if not filter_sets:
            filter_sets = {"": {}}
        for filter_set_name, filter_set in filter_sets.items():
            found = [process for process in psutil.process_iter() if _is_valid_system_process(process, process_name, filter_set)]
            val.dispatch(plugin=PLUGIN_NAME, plugin_instance=process_name, type='gauge', type_instance=filter_set_name, values=[len(found)], interval=config.interval)


def _is_valid_java_process(process, process_pattern):
    """"Checks whether the specified java process meets the criteria required for it
    :param process: the process to be checked
    :type process: str
    :param process_pattern: the pattern the process has to meet
    :type process_pattern: Regular Expression Object
    :returns: Whether the method meets the required criteria or not
    :rtype: bool"""

    if not process_pattern.search(process.lower()):
        return False
    return True


def _dispatch_java_processes(config, val):
    """Makes val dispatch all java processes that meet the criteria specified in config"""

    running_java_procs = _get_all_running_java_processes_args_iter()
    for process_regexp in config.java_processes:
        process_pattern = re.compile(process_regexp)
        found = [process for process in running_java_procs if _is_valid_java_process(process, process_pattern)]
        val.dispatch(plugin=PLUGIN_NAME, plugin_instance=_get_process_name_from_regex(process_regexp), type='gauge', values=[len(found)], interval=config.interval)


def read_callback(config):
    val = collectd.Values()
    try:
        _dispatch_system_processes(config, val)
    except Exception as exc:
        collectd.error(str(exc))

    try:
        if config.java_processes:
            _dispatch_java_processes(config, val)
    except Exception as exc:
        collectd.error(str(exc))


def process_configuration(filtering_file):
    """Processes the configuration file and returns the list of processes in it

    :param filtering_file: The path to the yaml file were the processes and filters are specified
    :type filtering_file: str
    :returns: A dictionary containing the processes and the sets of filters assigned to it
    :rtype: defaultdict"""

    processes = defaultdict(lambda: defaultdict(lambda: defaultdict(list)))

    if filtering_file is None:
        return processes

    try:
        with open(filtering_file, 'r') as file:
            try:
                content = file.read()
                data = yaml.safe_load(content)
                if data is not None:
                    # Backwards compatibility to be retired in the future
                    processes = data['processes'] if ('processes' in data) else data
                    for process, filter_sets in processes.items():
                        process_filter_sets = defaultdict(lambda: defaultdict(list))
                        if filter_sets is not None:
                            for filter_set_name, filter_set in filter_sets.items():
                                if filter_set_name != LEGACY_PPID_FILTER_NAME:
                                    processed_filter_set = defaultdict(list)
                                    for filter_name, filter_values in filter_set.items():
                                        if filter_name.lower() in FILTERS and filter_values:
                                            processed_filter_set[filter_name].extend(set(filter_values))
                                        else:
                                            collectd.warning('%s plugin: [Warning] Unknown filter specified in %s: %s' % (PLUGIN_NAME, filtering_file, filter_name))
                                    process_filter_sets[filter_set_name].update(processed_filter_set)
                                else:
                                    collectd.warning('%s plugin: [Warning] Attempted to override special filter set name %s in %s' % (PLUGIN_NAME, filter_set_name, filtering_file))
                        processes[process] = process_filter_sets
            finally:
                file.close()
    except IOError as e:
        collectd.warning("%s plugin: [Error] Couldn't open file %s: %s" % (PLUGIN_NAME, filtering_file, str(e)))
    except yaml.scanner.ScannerError as e:
        collectd.warning('%s plugin: [Error] Scanner error while parsing file %s: %s' % (PLUGIN_NAME, filtering_file, str(e)))
    except Exception as e:
        collectd.warning('%s plugin: [Error] %s occurred while reading the configuration from %s: %s' % (PLUGIN_NAME, type(e).__name__, filtering_file, str(e)))

    return processes


def load_processes(filtering_file, old_processes, ppid):
    """Loads the file's processes and joins them with the legacy ones to return a unified dict containing them all

    :param filtering_file: The path to the yaml file were the processes and filters are specified
    :type filtering_file: str
    :param old_processes: list of processes specified in the config in the legacy way
    :type old_processes: list[str]
    :param ppid: legacy general ppid specified for the full plugin; used for the old processes
    :type ppid: int
    :returns: a dict containing all processes and its filters
    :rtype: defaultdict[str, list[]]
    """

    processes = process_configuration(filtering_file)
    filter_sets = defaultdict(lambda: defaultdict(list))

    if ppid is not None:
        filter_set = defaultdict(list)
        filter_set['ppid'] = [ppid]
        filter_sets[LEGACY_PPID_FILTER_NAME] = filter_set

    for process in old_processes:
        processes[process] = filter_sets

    return processes


def configure_callback(conf):
    config = ProcesscountConfig()
    old_processes = []
    parentpid = None
    filtering_file = None

    for node in conf.children:
        try:
            key = node.key.lower()
            if key == 'process':
                old_processes += node.values
            elif key == 'javaprocess':
                config.java_processes += node.values
            elif key == 'parentpid':
                parentpid = int(node.values[0])
            elif key == 'interval':
                interval = int(node.values[0])
                config.interval = interval if interval > 0 else CONFIG_DEFAULT_INTERVAL
            elif key == 'filteringfilepath':
                filtering_file = node.values[0]
        except ValueError as e:
            collectd.warning('%s plugin: [Error] Value error in %s: %s' % (PLUGIN_NAME, node.key, str(e)))
        except Exception as e:
            collectd.warning('%s plugin: [Error] Configuration error in %s: %s' % (PLUGIN_NAME, node.key, str(e)))

    config.processes = load_processes(filtering_file, old_processes, parentpid)

    collectd.info('%s plugin: Configured with processes %s' % (PLUGIN_NAME, list(config.processes.keys()) + config.java_processes))
    if config.interval > 0:
        collectd.register_read(callback=read_callback, data=config, name=config.config_name, interval=config.interval)
    else:
        collectd.register_read(callback=read_callback, data=config, name=config.config_name)


collectd.register_config(configure_callback)
