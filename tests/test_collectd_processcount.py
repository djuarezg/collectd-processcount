import os
from collections import namedtuple, defaultdict
from random import randint

import pytest
from mock import MagicMock, Mock, patch

puids = namedtuple('puids', ['real', 'effective', 'saved'])


@pytest.fixture
def collectd_processcount():
    collectd = MagicMock()
    with patch.dict('sys.modules', {'collectd': collectd}):
        import collectd_processcount
        yield collectd_processcount


def test_plugin_registration(collectd_processcount):
    collectd_processcount.collectd.register_config_assert_called_once_with(collectd_processcount.configure_callback)


def test_configure_defaults(collectd_processcount):
    with patch('collectd.register_read') as register_mock:
        collectd_processcount.configure_callback(Mock(children=[]))

        collectd_processcount.collectd.register_read.assert_called_once()

        args_sent = register_mock.call_args[1]
        probe_config = args_sent['data']

        assert probe_config.processes == defaultdict(lambda: defaultdict(lambda: defaultdict(list)))
        assert probe_config.java_processes == []
        assert probe_config.interval == collectd_processcount.CONFIG_DEFAULT_INTERVAL
        assert args_sent['name'] == probe_config.config_name
        assert args_sent['callback'] == collectd_processcount.read_callback


@pytest.mark.parametrize("proc_single,proc_multi,java_single_proc,java_multi_proc,ppid", [
    (["proc_a", "proc_b"], ["proc_c", "proc_d"], ["java_single_proc"], ["java_proc_a", "java_proc_b"], 1)])
def test_configure_valid(collectd_processcount, proc_single, proc_multi, java_single_proc, java_multi_proc, ppid):
    with patch('collectd.register_read') as register_mock:
        collectd_processcount.configure_callback(Mock(children=
                                                      [Mock(key='Process', values=proc_single)] +
                                                      [Mock(key='Process', values=[proc]) for proc in proc_multi] +
                                                      [Mock(key='JavaProcess', values=java_single_proc)] +
                                                      [Mock(key='JavaProcess', values=[java_proc]) for java_proc in
                                                       java_multi_proc] +
                                                      [Mock(key='ParentPid', values=str(ppid))]
                                                      ))

        collectd_processcount.collectd.register_read.assert_called_once()

        args_sent = register_mock.call_args[1]
        probe_config = args_sent['data']
        expected_processes = _create_process_dict_from_array(collectd_processcount, proc_single + proc_multi, ppid)

        assert probe_config.processes == expected_processes
        assert probe_config.java_processes == java_single_proc + java_multi_proc

        assert args_sent['name'] == probe_config.config_name
        assert args_sent['callback'] == collectd_processcount.read_callback


@pytest.mark.parametrize("processes,times", [
    (["proc_a", "proc_b", "proc_c", "proc_d"], 0),
    (["proc_a", "proc_b", "proc_c", "proc_d"], 2)
])
def test_no_ppid_read_valid(collectd_processcount, processes, times):
    plugin_config = collectd_processcount.ProcesscountConfig()
    plugin_config.processes = _create_process_dict_from_array(collectd_processcount, processes)

    psutil_return_value = _create_psutil_return_value(processes)

    with patch('psutil.process_iter', return_value=psutil_return_value * times):
        with patch('collectd.Values') as val_mock:
            collectd_processcount.read_callback(plugin_config)
            val_mock.assert_called_once()
            for p in processes:
                val_mock.return_value.dispatch.assert_any_call(
                    plugin=collectd_processcount.PLUGIN_NAME,
                    plugin_instance=p,
                    type_instance="",
                    type='gauge',
                    values=[times],
                    interval=plugin_config.interval
                )


@pytest.mark.parametrize("processes,ppid", [
    (["proc_a", "proc_b", "proc_c", "proc_d"], 1),
    (["proc_a", "proc_b", "proc_c", "proc_d"], 2)
])
def test_with_ppid_read_valid(collectd_processcount, processes, ppid):
    plugin_config = collectd_processcount.ProcesscountConfig()
    plugin_config.processes = _create_process_dict_from_array(collectd_processcount, processes, ppid)

    psutil_return_value = _create_psutil_return_value(processes, ppid=ppid)                     # Expected
    psutil_return_value += _create_psutil_return_value(processes, ppid=ppid + randint(1, 100))  # Other

    with patch('psutil.process_iter', return_value=psutil_return_value):
        with patch('collectd.Values') as val_mock:
            collectd_processcount.read_callback(plugin_config)

            val_mock.assert_called_once()

            for p in processes:
                val_mock.return_value.dispatch.assert_any_call(
                    plugin=collectd_processcount.PLUGIN_NAME,
                    plugin_instance=p,
                    type='gauge',
                    type_instance=collectd_processcount.LEGACY_PPID_FILTER_NAME,
                    values=[1],
                    interval=plugin_config.interval
                )


@pytest.mark.parametrize("java_processes", [(["proc_a", "proc_b", "proc_d"]),
                                            (["test.*proc_a", "java.*proc-c"])])
def test_with_java_processes(collectd_processcount, java_processes):
    plugin_config = collectd_processcount.ProcesscountConfig()
    plugin_config.java_processes = java_processes

    running_java_procs_return_value = '\n'.join(["App -conf /test/proc_a/conf", "Proc_b --name test",
                                                 "Java --name proc-c", "TestProc --conf/process.yml"])

    with patch('subprocess.Popen.communicate', return_value=(running_java_procs_return_value, None)):
        with patch('collectd.Values') as val_mock:
            collectd_processcount.read_callback(plugin_config)
            val_mock.assert_called_once()

            for p in java_processes:
                val_mock.return_value.dispatch.assert_any_call(
                    plugin=collectd_processcount.PLUGIN_NAME,
                    plugin_instance=p.replace('.*', ''),
                    type='gauge',
                    values=([0] if p == "proc_d" else [1]),
                    interval=plugin_config.interval
                )


def test_should_warn_on_java_process_status_error(collectd_processcount):
    plugin_config = collectd_processcount.ProcesscountConfig()
    plugin_config.java_processes = ["test"]

    stderr_message = "exception"

    with patch('subprocess.Popen.communicate', return_value=(None, stderr_message)):
        with patch('collectd.Values') as val_mock:
            collectd_processcount.read_callback(plugin_config)
            val_mock.assert_called_once()
            val_mock.return_value.assert_not_called()


@pytest.mark.parametrize("filtering_file_path", [[os.path.join(os.path.abspath(os.path.dirname(__file__)), 'resources', 'processcount.yaml')]])
def test_configuration_from_filtering_file(collectd_processcount, filtering_file_path):
    with patch('collectd.register_read') as register_mock:
        collectd_processcount.configure_callback(Mock(children=[Mock(key='FilteringFilePath', values=filtering_file_path)]))
        collectd_processcount.collectd.register_read.assert_called_once()

        args_sent = register_mock.call_args[1]
        probe_config = args_sent['data']

        expected_processes = {
            'proc_a': {'set_1': {'ppid': [0], 'uid': [1, 3]}, 'set_2': {'uid': [498]}},
            'proc_b': {'set_1': {'ppid': [123]}},
            'proc_c': {},
            'proc_d': {'regex': {'command_line': ['.*procd.*']}},
        }

        assert probe_config.processes == expected_processes
        assert args_sent['name'] == probe_config.config_name
        assert args_sent['callback'] == collectd_processcount.read_callback


@pytest.mark.parametrize("process_name,type_instance,filters,matches", [
    ('proc_a', 'set_1', {'ppid': [0], 'uid': [1, 3]}, 2),
    ('proc_a', 'set_2', {'uid': [498]}, 2),
    ('proc_b', 'set_1', {'ppid': [123]}, 2),
    ('proc_c', '', {}, 11),
    ('proc_d', 'regex', {'command_line': ['.*procd.*']}, 1)
])
def test_new_processes_and_filtering(collectd_processcount, process_name, type_instance, filters, matches):
    plugin_config = collectd_processcount.ProcesscountConfig()
    plugin_config.processes = { process_name: { type_instance: filters } }

    psutil_return_value = _create_psutil_return_value([process_name], ppid=0, uid=1)     # Expected for proc_a-set_1
    psutil_return_value += _create_psutil_return_value([process_name], ppid=0, uid=3)    # Expected for proc_a-set_1
    psutil_return_value += _create_psutil_return_value([process_name], ppid=0, uid=2)    # Unexpected for proc_a-set_1

    psutil_return_value += _create_psutil_return_value([process_name], ppid=0, uid=498)        # Expected for proc_a-set_2
    psutil_return_value += _create_psutil_return_value([process_name], ppid=12345, uid=498)    # Expected for proc_a-set_2
    psutil_return_value += _create_psutil_return_value([process_name], ppid=0, uid=111)        # Unexpected for proc_a-set_2

    psutil_return_value += _create_psutil_return_value([process_name], ppid=123, uid=12)       # Expected for proc_b
    psutil_return_value += _create_psutil_return_value([process_name], ppid=123, uid=21)       # Expected for proc_b
    psutil_return_value += _create_psutil_return_value([process_name], ppid=0, uid=21)         # Unexpected for proc_b

    psutil_return_value += _create_psutil_return_value([process_name], command_line=['bin', 'procd', '--valid']) # Expected for proc_d
    psutil_return_value += _create_psutil_return_value([process_name], command_line=['bin', 'proc', '--novalid']) # Unexpected for proc_d

    with patch('psutil.process_iter', return_value=psutil_return_value):
        with patch('collectd.Values') as val_mock:
            collectd_processcount.read_callback(plugin_config)
            val_mock.assert_called_once()
            val_mock.return_value.dispatch.assert_called_once_with(
                plugin=collectd_processcount.PLUGIN_NAME,
                plugin_instance=process_name,
                type='gauge',
                type_instance=type_instance,
                values=[matches],
                interval=plugin_config.interval
            )


def _create_process_dict_from_array(collectd_processcount, processes, ppid=None, command_line=[]):
    expected_processes = defaultdict(lambda: defaultdict(lambda: defaultdict(list)))

    for process in processes:
        filter_sets = defaultdict(lambda: defaultdict(list))

        if ppid is not None:
            filter_set = defaultdict(list)
            filter_set['ppid'] = [ppid]
            filter_sets[collectd_processcount.LEGACY_PPID_FILTER_NAME] = filter_set

        expected_processes[process] = filter_sets

    return expected_processes


def _create_psutil_return_value(processes, uid=randint(-2, 100), ppid=randint(0, 10000), command_line=[]):
    psutil_return_value = []
    for p in processes:
        p_mock = Mock()
        uids = puids(uid, uid, uid)
        p_mock.configure_mock(name=p, uids=uids, ppid=ppid, cmdline=command_line)
        psutil_return_value.append(p_mock)

    return psutil_return_value
