import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.rst')).read()
NEWS = open(os.path.join(here, 'NEWS.txt')).read()

version = '2.6.1'

install_requires = [
    'psutil',
    'pyyaml'
]


setup(name='collectd_processcount',
    version=version,
    description="Collectd Plugin to count processes",
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: System Administrators",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python",
        "Topic :: System :: Monitoring",
    ],
    keywords='collectd process monitoring',
    url = 'https://gitlab.cern.ch/monitoring/collectd-processcount',
    author='Luis Fenandez Alvarez',
    author_email='luis.fernandez.alvarez@cern.ch',
    maintainer='CERN IT Monitoring',
    maintainer_email='monit-support@cern.ch',
    license='Apache II',
    packages=find_packages('src'),
    package_dir = {'': 'src'},
    include_package_data=True,
    zip_safe=False,
    install_requires=install_requires
)
