# Created by pyp2rpm-3.3.2
%global pypi_name collectd_processcount

Name:           python-%{pypi_name}
Version:        2.6.1
Release:        1%{?dist}
Summary:        Collectd Plugin to count processes

License:        Apache II
URL:            https://gitlab.cern.ch/monitoring/collectd-processcount
Source0:        https://gitlab.cern.ch/monitoring/collectd-processcount/-/archive/%{version}/collectd-processcount-%{version}.tar.gz
BuildArch:      noarch

%if 0%{?el6}
%global py_version python2
%define __python /usr/bin/python2
BuildRequires:  python2-devel
BuildRequires:  python-setuptools
%else
%if 0%{?el7}
%global py_version python2
%define __python /usr/bin/python2
BuildRequires:  python2-devel
BuildRequires:  python-setuptools
%else
%if 0%{?el8}
%global py_version python3
%define __python /usr/bin/python3
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
%endif
%endif
%endif

%description

%package -n     %{py_version}-%{pypi_name}
Summary:        %{summary}
%{?python_provide:%python_provide %{py_version}-%{pypi_name}}

%{?el6:Requires: python-psutil}
%{?el7:Requires: python2-psutil}
%{?el8:Requires: python3-psutil}

%description -n %{py_version}-%{pypi_name}

%prep
%autosetup -n collectd-processcount-%{version}
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info

%build
%{__python} setup.py build

%install
%{__python} setup.py install --skip-build --root %{buildroot}

%post -n %{py_version}-%{pypi_name}
/sbin/service collectd condrestart >/dev/null 2>&1 || :

%postun -n %{py_version}-%{pypi_name}
if [ $1 -eq 0 ]; then
    /sbin/service collectd condrestart >/dev/null 2>&1 || :
fi

%files -n %{py_version}-%{pypi_name}
%doc README.rst NEWS.txt LICENSE
%{python_sitelib}/%{pypi_name}
%{python_sitelib}/%{pypi_name}-%{version}-py?.?.egg-info

%changelog
* Thu Mar 26 2020 Simone Brundu <simone.brundu@cern.ch> - 2.6.1-1
- [MONIT-2463] Fix rpm Requirements

* Tue Feb 18 2020 Simone Brundu <simone.brundu@cern.ch> - 2.6.0-1
- [MONIT-2389] Adapted package for Python 3 and Centos 8

* Thu Aug 15 2019 Borja Garrido Bear <borja.garrido.bear@cern.ch> - 2.5.0-1
- [MONIT-2166] Add command line filtering option

* Wed May 22 2019 Gonzalo Menéndez Borge <gonzalo.menendez.borge@cern.ch> - 2.4.2-1
- [MONIT-2042] Delete default value for the filtering file

* Thu Apr 11 2019 Borja Garrido Bear <borja.garrido.bear@cern.ch> - 2.4.1-1
- [NOTICKET] Fix default interval proving

* Fri Apr 05 2019 Gonzalo Menéndez Borge <gonzalo.menendez.borge@cern.ch> - 2.4.0-1
- [MONIT-1980] Support filtering by user ID (UID) + allow filtering per process

* Tue Mar 12 2019 Borja Garrido Bear <borja.garrido.bear@cern.ch> - 2.3.1-1
- [MONIT-1963] Add big error catch to avoid Collectd receiving it

* Wed Oct 24 2018 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 2.3.0-2
- New package release after some problems in the build process

* Wed Oct 24 2018 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 2.3.0-1
- [MONIT-1822] Allow multiple plugin configurations
- Dispatch the interval with the metrics

* Wed Oct 10 2018 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 2.2.2-1
- [MONIT-1763] Avoid java path if no java processes are configured 

* Fri Sep 28 2018 Borja Garrido Bear <borja.garrido.bear@cern.ch> - 2.2.1-1
- [MONIT-1725] Remove replacement of dash character in the plugin instance

* Fri Aug 17 2018 Nikolay Tsvetkov <n.tsvetkov@cern.ch> - 2.2.0-1
- Add support for counting specific Java process using pattern matching on the arguments

* Fri Jul 20 2018 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 2.1.0-1
- Add a global ParentPid option to filter processes

* Mon Jul  9 2018 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 2.0.0-1
- Change the metric type to gauge as it is more appropriate with the scope of the plugin.

* Mon Jun 25 2018 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 1.0.0-1
- Initial package.
