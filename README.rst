Processcount Collectd Plugin
============================

Configuration
-------------

Example::

    <Plugin "python">

      Import "collectd_processcount"

      <Module "collectd_processcount">
        Process "sshd"
        Process "rpm" "yum"

        JavaProcess "kafka"
        JavaProcess "logstash" "flume.*agent-test"

        ParentPid 1

        Interval "300"

        FilteringFilePath "/etc/collectd.d/processcount/processcount.yaml"
      </Module>

    </Plugin>


* ``Process``: process name(s) to be counted.
* ``JavaProcess``: pattern to be counted in the running java process arguments. This parameter accepts regex patterns which
    will be used to match the processes, but while building the metrics name all the special characters will be discarded.

    IMPORTANT: Enabling this option requires existence of 'java' module in the "pluginsync" filter of your hostgroup as
    Java tools are used on the machine in order to obtain the necessary information. For that purpose OpenJDK Devel package
    will be also installed on your machine if not already present there.
* ``ParentPid``: Optional parameter defining the parent PID to filter (applies for all "Process", but not "JavaProcess").
* ``Interval``: interval in seconds to get the metrics (applies to all processes).
* ``FilteringFilePath``: path to the filtering yaml file. Processes can be specified in said file in its "processes" section, as well as sets of filters per process if so desired. The file must follow the following structure::

    processes:
      process:
        filter_set:
          filter: [values]

An example yaml file may be found below::

    processes:
      process_a:
      process_b:
        myfilters:
          ppid: [0, 1]
          uid: [100]
        otherfilters:
          ppid: [12345]
      process_c:
        myfilters:
          uid: [12, 32, 43]
      process_d:
        myfilters:
          command_line: ['.*something.*']

The above yaml file will configure the plugin to filter:

* "process_a" without any filters
* "process_b" if the "ppid" is 0 or 1 and the "uid" is 100, which will be identified as myfilters
* "process_b" if the "ppid" is 12345, which will be identified as otherfilters
* "process_c" if the "uid" is 12, 32 or 43, which will be identified as myfilters
* "process_d" if the "command_line" as reported by psutil matches the regexp '.*something.*'

Metrics
-------

The metrics are published in the following structure::

    Plugin: processcount
    PluginInstance: <process>
    Type: gauge
    TypeInstance: <Filter set name> (if any; e.g. "myfilters" or "otherfilters" in the example above)

Example::

    node123.cern.ch/processcount-rpm/gauge values=[2]
    node123.cern.ch/processcount-yum/gauge values=[0]
    node123.cern.ch/processcount-sshd/gauge values=[1]
    node123.cern.ch/processcount-kafka/gauge values=[2]
    node123.cern.ch/processcount-logstash/gauge values=[0]
    node123.cern.ch/processcount-flumeagenttest/gauge values=[1]
    node123.cern.ch/processcount-python/gauge-myfilters values=[1]
    node123.cern.ch/processcount-python/gauge-otherfilters values=[0]

